import pygame
import time
import sys
import RPi.GPIO as gpio

sys.path.append('../I32CTT/python')
from phy.at86rf233_rpi.driver import driver_at86rf233
from mac.ieee802154.driver import driver_ieee802154
from i32ctt.driver import driver_i32ctt

#Poligonos empleados para dibujar cada segmento.
segmento_a = [(0.25, 0.1), (0.35, 0.05), (0.65, 0.05), (0.75, 0.1), (0.65, 0.15), (0.35, 0.15)]
segmento_b = [(0.8, 0.125), (0.9, 0.175), (0.9, 0.425), (0.8, 0.475), (0.7, 0.425), (0.7, 0.175)]
segmento_c = [(0.8, 0.525), (0.9, 0.575), (0.9, 0.825), (0.8, 0.875), (0.7, 0.825), (0.7, 0.575)]
segmento_d = [(0.25, 0.9), (0.35, 0.85), (0.65, 0.85), (0.75, 0.9), (0.65, 0.95), (0.35, 0.95)]
segmento_e = [(0.2, 0.525), (0.3, 0.575), (0.3, 0.825), (0.2, 0.875), (0.1, 0.825), (0.1, 0.575)]
segmento_f = [(0.2, 0.125), (0.3, 0.175), (0.3, 0.425), (0.2, 0.475), (0.1, 0.425), (0.1, 0.175)]
segmento_g = [(0.25, 0.5), (0.35, 0.45), (0.65, 0.45), (0.75, 0.5), (0.65, 0.55), (0.35, 0.55)]
segmento_dp = [(0.3, 0.9), (0.7, 0.9), (0.7, 1.0), (0.3, 1.0)]  #Punto decimal
segmento_c1 = [(0.3, 0.2), (0.7, 0.2), (0.7, 0.3), (0.3, 0.3)]  #Dos puntos (punto de arriba)
segmento_c2 = [(0.3, 0.7), (0.7, 0.7), (0.7, 0.8), (0.3, 0.8)]  #Dos puntos (punto de abajo)

#Funcion usada para dibujar digitos estilo siete segmetos. La funcion dibuja numeros regulares
#cuando se pasan los valores de 0 a 9 en el parametro de digito. El valor 10 dibuja un punto decimal
#(.) y el valor 11 dibuja dos puntos (:).
def dibujar_digito(superficie, x, y, ancho, alto, digito):
  #Esqueleto de referencia. Descomentar para ajustar coordenadas.
  #pygame.draw.polygon(superficie, color_texto, [(x, y), (x + ancho, y), (x + ancho, y + alto), (x, y + alto)], 1)
  #pygame.draw.line(superficie, color_texto, (x, y), (x + ancho, y + alto * 0.5))
  #pygame.draw.line(superficie, color_texto, (x + ancho, y), (x, y + alto * 0.5))
  #pygame.draw.line(superficie, color_texto, (x, y + alto * 0.6), (x + ancho, y + alto * 0.1))
  #pygame.draw.line(superficie, color_texto, (x + ancho, y + alto * 0.6), (x, y + alto * 0.1))
  #pygame.draw.line(superficie, color_texto, (x, y + alto * 0.4), (x + ancho, y + alto * 0.9))
  #pygame.draw.line(superficie, color_texto, (x+ ancho, y + alto * 0.4), (x, y + alto * 0.9))
  #pygame.draw.line(superficie, color_texto, (x, y + alto * 0.5), (x + ancho, y + alto))
  #pygame.draw.line(superficie, color_texto, (x + ancho, y + alto * 0.5), (x, y + alto))
  if digito == 0:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_a])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_b])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_d])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_e])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_f])
  if digito == 1:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_b])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
  elif digito == 2:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_a])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_b])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_d])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_e])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_g])
  elif digito == 3:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_a])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_b])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_d])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_g])
  elif digito == 4:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_b])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_f])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_g])
  elif digito == 5:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_a])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_d])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_f])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_g])
  elif digito == 6:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_a])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_d])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_e])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_f])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_g])
  elif digito == 7:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_a])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_b])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
  elif digito == 8:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_a])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_b])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_d])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_e])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_f])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_g])
  elif digito == 9:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_a])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_b])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_d])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_f])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_g])
  elif digito == 10:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_dp])
  elif digito == 11:
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c1])
    pygame.draw.polygon(superficie, color_texto, [(i[0]*ancho+x, i[1]*alto+y) for i in segmento_c2])

#Funcion de dibujado de tiempo total.
def dibujar_tiempo_total(superficie, x, y, ancho, alto, tiempo):
  #Se divide la superficie en 6 secciones de igual anchura para cada uno de los 5 digitos. El
  #espacio restante se emplea para los caracteres de separacion (dos puntos y punto). El valor de
  #cada digito se extrae usando aritmetica modular.
  dibujar_digito(superficie, x,                   y, ancho / 6,  alto, tiempo / 60000)
  dibujar_digito(superficie, x + ancho * 1 / 6,   y, ancho / 12, alto, 11)
  dibujar_digito(superficie, x + ancho * 1.5 / 6, y, ancho / 6,  alto, tiempo / 10000 % 6)
  dibujar_digito(superficie, x + ancho * 2.5 / 6, y, ancho / 6,  alto, tiempo / 1000 % 10)
  dibujar_digito(superficie, x + ancho * 3.5 / 6, y, ancho / 12, alto, 10)
  dibujar_digito(superficie, x + ancho * 4 / 6,   y, ancho / 6,  alto, tiempo / 100 % 10)
  dibujar_digito(superficie, x + ancho * 5 / 6,   y, ancho / 6,  alto, tiempo / 10 % 10)

#Funcion de formateo de tiempos.
def formatear_tiempo(tiempo):
  #Divide cada digito usando aritmetica modular y lo anexa a la cadena de texto.
  cadena = str(tiempo / 60000)        #Digito de minutos
  cadena += ":"                       #Separador de segundos
  cadena += str(tiempo / 10000 % 6)   #Digito de decenas de segundos
  cadena += str(tiempo / 1000 % 10)   #Digito de unidades de segundos
  cadena += "."                       #Separador de centesimas
  cadena += str(tiempo / 100 % 10)    #Digito de decenas para las centesimas de segundo
  cadena += str(tiempo / 10 % 10)     #Digito de unidades para las centesimas de segundo
  return cadena

#Funcion de actualizacion de pantalla.
def actualizar_pantalla(tiempo_total, tiempo_vuelta, limite_vueltas, paro_manual):
  #Limpia la superficie primaria utilizando el color de fondo.
  superf_pant.fill(color_fondo)

  #Dibuja primero el tiempo total con estilo de display de 7 segmentos.
  dibujar_tiempo_total(superf_pant, 0, 0, superf_pant.get_width(), altura_fila_superior, tiempo_total)

  #Determina la cantidad de filas que aparecen con los tiempos de vuelta.
  num_filas = limite_vueltas / 3 + (0 if limite_vueltas % 3 == 0 else 1)

  #Calcula las coordenadas iniciales. La posicion en X se ajusta de forma que queden 3 columnas
  #centradas. La posicion en Y sube una fila por cada una que se agregue.
  x_pos = superf_pant.get_width() / 6
  y_pos = altura_fila_superior + (3 - num_filas) * altura_fila_inferior

  #Itera imprimiendo los tiempos de vuelta.
  for i in range(limite_vueltas):
    #Genera la superficie para la etiqueta de vuelta y la copia a la superficie primaria. Esta se
    #imprime con justificacion a la derecha, por el lado izquierdo de la coordenada en X.
    texto = "Vuelta " + str(i + 1) + ":  "
    superficie = fuente_etiqueta.render(texto, True, color_texto, color_fondo)
    superf_pant.blit(superficie, (x_pos - superficie.get_width(), y_pos))

    #Genera la superficie para el tiempo de vuelta, si esta disponible, y la copia a la superficie
    #primaria. Esta se imprime con justificacion a la izquierda, por el lado derecho de X.
    if i < len(tiempo_vuelta):
      texto = formatear_tiempo(tiempo_vuelta[i])
      superficie = fuente_tiempo.render(texto, True, color_texto, color_fondo)
      superf_pant.blit(superficie, (x_pos, y_pos))

    #Avanza la posicion en X un tercio de la anchura de la pantalla.
    x_pos += superf_pant.get_width() / 3

    #Se ajustan las coordenadas al alcanzar el final de la fila.
    if i % 3 == 2:
      x_pos = superf_pant.get_width() / 6
      y_pos += altura_fila_inferior

  #Dibuja la advertencia parpadeante de paro manual si la bandera esta activa.
  if paro_manual:
    #Determina si la ventana de tiempo de parpadeo cada segundo esta activa.
    if int(time.time() * 10) % 10 < 5:
      #Si esta activa, renderea el mensaje en una superficie temporal.
      superficie = fuente_etiqueta.render("--- Paro manual ---", True, color_texto, color_fondo)

      #Calcula la posicion del texto centrado en la pantalla y al fondo.
      x_pos = (superf_pant.get_width() - superficie.get_width()) / 2
      y_pos = altura_fila_superior + altura_fila_inferior * 4

      #Dibuja la superficie con el texto sobre la superficie primaria.
      superf_pant.blit(superficie, (x_pos, y_pos))

  #Actualiza la pantalla con la superficie primaria finalizada.
  pygame.display.flip()

#Inicializa la pila de protocolos de I32CTT.
phy = driver_at86rf233(gpio)
mac = driver_ieee802154(phy)
i32ctt = driver_i32ctt(mac)
mac.escr_config_red(canal = 26, pan_id = 0xBABE, dir_corta = 0x0200)

#Se inicializa pygame.
pygame.init()

#Se establece el modo de video en automatico y se obtiene la superficie de la pantalla primaria.
superf_pant = pygame.display.set_mode()

#Apaga el cursor del mouse.
pygame.mouse.set_visible(False)

#Se divide la pantalla en la fila superior, que contiene el tiempo total dibujado en estilo de 7
#segmentos y 5 filas inferiores, que contienen los tiempos de vuelta y la advertencia parpadeante de
#paro manual.
altura_fila_superior = superf_pant.get_height() * 2 / 3
altura_fila_inferior = (superf_pant.get_height() - altura_fila_superior) / 5

#Se obtiene la fuente por defecto para las etiquetas.
fuente_etiqueta = pygame.font.SysFont(None, altura_fila_inferior)

#Se obtiene una fuente mono espaciada para los tiempos de vuelta. Se usa la misma altura de la
#las etiquetas, porque no siempre se obtiene la altura exacta y para garantizar que sean iguales.
fuente_tiempo = pygame.font.Font("/usr/share/fonts/truetype/freefont/FreeMono.ttf", fuente_etiqueta.get_height())

#Colores usados para dibujar los elementos de pantalla.
color_fondo = pygame.Color(0, 0, 0)
color_texto = pygame.Color(255, 255, 255)

while True:
  #Se intenta leer los registros 0 y 1, que son de estado y tiempo total.
  pares = i32ctt.leer_registros(0x0100, 0, [0, 1])

  #Si no se pudo leer, se repite el lazo para reintentarlo.
  if not pares:
    continue

  #Se extrae el registro de estado y el tiempo total de los pares de registros.
  reg_estado = pares[0][1]
  tiempo_total = pares[1][1]

  #Se extrae del registro de estado los campos de bits.
  limite_vueltas = reg_estado & 0xF
  conteo_vueltas = (reg_estado >> 4) & 0xF
  paro_manual = True if reg_estado & 0x100 != 0 else False

  #Se inicia con la lista vacia de tiempos de vuelta.
  tiempo_vuelta = []

  #Verifica si el conteo de vueltas no esta a 0.
  if conteo_vueltas > 0:
    #Si no lo esta, se leen los registros a partir del 2, para la cantidad de vueltas que indique
    #el conteo de vueltas.
    pares = i32ctt.leer_registros(0x0100, 0, range(2, 2 + conteo_vueltas))
    if pares:
      #Si la lecura fue exitosa, se anexan los tiempos de vuelta obtenidos.
      for par in pares:
        tiempo_vuelta.append(par[1])

  #Actualiza la pantalla.
  actualizar_pantalla(tiempo_total, tiempo_vuelta, limite_vueltas, paro_manual)

  #Se manejan los eventos de pygame.
  for event in pygame.event.get():
    #Si el evento es el de finalizacion o se presiona la tecla Q, el programa se finaliza.
    if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_q):
      #Limpia el estado del driver de gpio antes de salir.
      gpio.cleanup()
      exit()
