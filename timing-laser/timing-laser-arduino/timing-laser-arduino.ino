#include "src/at86rf233-iodriver-arduino.h"
#include "src/at86rf233-driver.h"
#include "src/phy-802154-driver.h"
#include "src/mac-802154-driver.h"
#include "src/i32ctt-slave.h"

#include "bubble_display.h"
#include "timer.h"

//Pin configurations.
const int open_analog_pin = A5;   //Used for random seed

const int radio_cs_pin = 8;       //AT86RF233 radio (SPI is shared with display)
const int radio_rst_pin = 6;
const int radio_slp_tr_pin = 7;
const int radio_irq_pin = 2;

const int display_cs = 10;        //Bubble display (SPI is shared with radio)

const int sensor_irq_pin = 3;     //Laser sensor receiver (LDR + debouncing circuit)

const int button_reset_pin = A2;  //On board buttons
const int button_lap_pin = A3;

const int led_0_pin = A0;         //On board LEDs (0: red/sensor state, 1: green/timer state)
const int led_1_pin = A1;

//802.15.4 network settings.
const uint8_t phy_channel = 26;
const uint16_t mac_pan_id = 0xBABE;
const uint16_t mac_short_addr = 0x0100;

//Display and timer instances.
BUBBLE_DISPLAY display(display_cs);
TIMER timer(display, led_1_pin);

//I32CTT slave callback for read register command.
void ep0_read_reg_callback(const i32ctt_addr_t *reg_addr_array, uint16_t reg_count) {
  i32ctt_reg_pair_t * const p_ans_buffer = i32ctt_slave_get_read_ans_buffer();  //Get answer buffer
  const uint16_t master_addr = mac_802154_driver_get_rx_source_addr();          //Get sender address
  uint16_t i;
  uint32_t data;
  uint16_t ans_count = 0;

  //Iterate through the I32CTT request. Read all requested registers and prepare the answer.
  for (i = 0; i < reg_count; i++) {
    if (timer.read_register(reg_addr_array[i].address, &data)) {
      p_ans_buffer[ans_count].address = reg_addr_array[i].address;
      p_ans_buffer[ans_count].data = data;
      ++ans_count;
    }
  }

  //Send the response if at least one register was read.
  if (ans_count > 0) {
    mac_802154_driver_set_tx_destination_addr(master_addr);   //Respond to sender
    i32ctt_slave_send_read_reg_ans(ans_count);
  }
}

//I32CTT slave instance. Only register reads are supported.
I32CTT_SLAVE_INSTANCE(ep0, 0, ep0_read_reg_callback, NULL);

//Laser sensor interrupt handler.
void sensor_irq_handler() {
  //Trigger the timer and update LED 0.
  timer.trigger();
  digitalWrite(led_0_pin, digitalRead(sensor_irq_pin));
}

void setup() {
  //Configure all pins.
  pinMode(sensor_irq_pin, INPUT_PULLUP);
  pinMode(button_reset_pin, INPUT_PULLUP);
  pinMode(button_lap_pin, INPUT_PULLUP);
  pinMode(led_0_pin, OUTPUT);
  pinMode(led_1_pin, OUTPUT);
  digitalWrite(led_0_pin, digitalRead(sensor_irq_pin));   //Update LED 0 for the first time

  //Seed the random number generator using an open analog channel.
  randomSeed(analogRead(open_analog_pin));

  //Initialize the I/O driver for the radio.
  at86rf233_iodriver_arduino_init(radio_cs_pin, radio_rst_pin, radio_slp_tr_pin, radio_irq_pin);

  //Initialize the AT86RF233 radio.
  at86rf233_driver_init(false);

  //Set the radio channel.
  phy_802154_driver_set_channel(phy_channel);

  //Initialize the MAC protocol driver.
  mac_802154_driver_init(mac_pan_id, mac_short_addr, random(256));

  //Add the slave instance to I32CTT.
  i32ctt_slave_add_instance(&ep0);

  //Initialize the display.
  display.begin();

  //Enable the laser sensor interrupt.
  attachInterrupt(digitalPinToInterrupt(sensor_irq_pin), sensor_irq_handler, CHANGE);
}

void loop() {
  bool button_reset_now;
  static bool button_reset_last;
  bool button_lap_now;
  static bool button_lap_last;
  unsigned long current_time;
  static unsigned long last_button_time = 0;

  //Update the radio and timer state.
  i32ctt_update();
  timer.update();

  //Capture the current system time.
  current_time = millis();

  //Update button logic periodically.
  if (current_time - last_button_time > 10) {
    //Read current button states.
    button_reset_now = digitalRead(button_reset_pin);
    button_lap_now = digitalRead(button_lap_pin);

    //Call the reset button event function if it was pressed.
    if (!button_reset_now && button_reset_last)
      timer.on_button_reset();

    //Call the lap button event function if it was pressed.
    if (!button_lap_now && button_lap_last)
      timer.on_button_lap();

    //Store previous button states.
    button_reset_last = button_reset_now;
    button_lap_last = button_lap_now;

    //Store last update time.
    last_button_time = current_time;
  }
}

