#include <Arduino.h>
#include <SPI.h>

#include "src/at86rf233-iodriver.h"

#include "bubble_display.h"

//Register addresses.
#define REG_DIGIT_0       0x01
#define REG_DIGIT_1       0x02
#define REG_DIGIT_2       0x03
#define REG_DIGIT_3       0x04
#define REG_DIGIT_4       0x05
#define REG_DIGIT_5       0x06
#define REG_DIGIT_6       0x07
#define REG_DIGIT_7       0x08
#define REG_DECODE_MODE   0x09
#define REG_INTENSITY     0x0A
#define REG_SCAN_LIMIT    0x0B
#define REG_SHUTDOWN      0x0C
#define REG_DISPLAY_TEST  0x0F

//Bit masks for individual segments (these are scrambled to optimize pcb routing).
#define SEG_A   0x80
#define SEG_B   0x08
#define SEG_C   0x02
#define SEG_D   0x10
#define SEG_E   0x40
#define SEG_F   0x04
#define SEG_G   0x01
#define SEG_DP  0x20

//Decode mode register value.
#define DECODE_MODE_NO_DECODE_ALL   0x00

//Intensity register value.
#define INTENSITY_VALUE   0x08

//Scan limit register value.
#define SCAN_LIMIT_8_DIGITS   0x07

//Shutdown register value.
#define SHUTDOWN_NORMAL_MODE  0x01

//Display test register value.
#define DISPLAY_TEST_NORMAL_OPERATION   0x00

BUBBLE_DISPLAY::BUBBLE_DISPLAY(int cs_pin):
  cs_pin(cs_pin), dp_state(0)
{
}

//Initialize the display.
void BUBBLE_DISPLAY::begin() {

  //Turn off automatic segment decoding (segments are scrambled to optimize pcb routing).
  write_register(REG_DECODE_MODE, DECODE_MODE_NO_DECODE_ALL);

  //Set display intensity to configured value.
  write_register(REG_INTENSITY, INTENSITY_VALUE);

  //Use all 8 digits.
  write_register(REG_SCAN_LIMIT, SCAN_LIMIT_8_DIGITS);

  //Disable shut down mode.
  write_register(REG_SHUTDOWN, SHUTDOWN_NORMAL_MODE);

  //Disable display test mode.
  write_register(REG_DISPLAY_TEST, DISPLAY_TEST_NORMAL_OPERATION);
}

void BUBBLE_DISPLAY::set_digit(uint8_t pos, uint8_t c) {
  //Set the digit. Overlap the decimal point segment if active.
  if (dp_state & (1 << pos))
    write_register(translate_position(pos), translate_character(c) | SEG_DP);
  else
    write_register(translate_position(pos), translate_character(c));
}

void BUBBLE_DISPLAY::set_dp(uint8_t pos, bool state) {
  //Modify and store the decimal point segment using bit masks.
  if (state)
    dp_state |= 1 << pos;
  else
    dp_state &= ~(1 << pos);
}

uint8_t BUBBLE_DISPLAY::translate_position(uint8_t pos) {
  //Map the logical digit position to hardware (cathodes are scrambled to optimize pcb routing).
  switch (pos) {
    case 0:  return REG_DIGIT_1;
    case 1:  return REG_DIGIT_0;
    case 2:  return REG_DIGIT_5;
    case 3:  return REG_DIGIT_7;
    case 4:  return REG_DIGIT_3;
    case 5:  return REG_DIGIT_4;
    case 6:  return REG_DIGIT_2;
    default: return REG_DIGIT_6;
  }
}

uint8_t BUBBLE_DISPLAY::translate_character(uint8_t c) {
  //Map the logical character code to a hardware segment encoding.
  switch (c) {
    case 0:   return SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F;
    case 1:   return         SEG_B | SEG_C;
    case 2:   return SEG_A | SEG_B |         SEG_D | SEG_E |         SEG_G;
    case 3:   return SEG_A | SEG_B | SEG_C | SEG_D |                 SEG_G;
    case 4:   return         SEG_B | SEG_C |                 SEG_F | SEG_G;
    case 5:   //same as 'S'.
    case 'S': return SEG_A |         SEG_C | SEG_D |         SEG_F | SEG_G;
    case 6:   return SEG_A |         SEG_C | SEG_D | SEG_E | SEG_F | SEG_G;
    case 7:   return SEG_A | SEG_B | SEG_C;
    case 8:   return SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G;
    case 9:   return SEG_A | SEG_B | SEG_C | SEG_D |         SEG_F | SEG_G;
    case 'A': return SEG_A | SEG_B | SEG_C |         SEG_E | SEG_F | SEG_G;
    case 'L': return                         SEG_D | SEG_E | SEG_F;
    case 'P': return SEG_A | SEG_B |                 SEG_E | SEG_F | SEG_G;
    case ' ': return 0;
    case '-': //Same as default/unknown character code.
    default:  return                                                 SEG_G;
  }
}

void BUBBLE_DISPLAY::write_register(uint8_t address, uint8_t data) {
  //Disable radio interrupts before accessing the shared SPI bus.
  at86rf233_iodriver.irq_enable(false);

  //Activate chip select.
  digitalWrite(cs_pin, LOW);

  //Send the address followed by the data.
  SPI.transfer(address);
  SPI.transfer(data);

  //Deactivate chip select.
  digitalWrite(cs_pin, HIGH);

  //Enable radio interrupts again.
  at86rf233_iodriver.irq_enable(true);
}

