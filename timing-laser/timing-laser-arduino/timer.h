#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>
#include "bubble_display.h"

class TIMER {
public:
  TIMER(BUBBLE_DISPLAY &display, int led_pin);
  void update();
  void trigger();
  void on_button_reset();
  void on_button_lap();
  bool read_register(uint16_t address, uint32_t *data);
private:
  void update_display_time(uint8_t lap, unsigned long time);
  void update_display_laps();

  enum TIMER_STATE {
    STATE_IDLE,   //Idle state. Lap limit can be configured from this state.
    STATE_RUN,    //Run state. Timer is actively updating the display.
    STATE_STOP,   //Stop state. Total time or lap time is shown in this state.
  };

  const int led_pin;

  BUBBLE_DISPLAY &display;          //Reference to display driver instance
  TIMER_STATE state;                //Current FSM state

  uint8_t lap_limit;                //Programmed lap limit
  uint8_t lap_count;                //Current lap count
  uint8_t time_shown;               //Index of time shown when in STOP state

  bool manual_stop;                 //Indicates wether the timer was stopped manually

  unsigned long last_trigger_time;  //Last trigger time
  unsigned long lap_time[11];       //Individual lap time
  //Notes:
  //- All times are in milliseconds.
  //- All times store raw millis() readings, which are taken upon the time of the event.
  //- Lap 0 time stores the starting time. It's not an actual lap time.
  //- All lap times are calculated as differences from the previous one.
  //- Total race time is calculated as a difference between lap 0 time and last lap time.
};

#endif //TIMER_H_
