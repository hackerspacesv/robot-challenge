#ifndef BUBBLE_DISPLAY_H_
#define BUBBLE_DISPLAY_H_

#include <stdint.h>

class BUBBLE_DISPLAY {
  public:
    BUBBLE_DISPLAY(int cs_pin);
    void begin();
    void set_digit(uint8_t pos, uint8_t c);
    void set_dp(uint8_t pos, bool state);
  private:
    uint8_t translate_position(uint8_t pos);
    uint8_t translate_character(uint8_t c);
    void write_register(uint8_t address, uint8_t data);

    const int cs_pin;   //Configured pin number for the MAX7219 chip select
    uint8_t dp_state;   //Bitfield that contains decimal point status for all digits
};

#endif //BUBBLE_DISPLAY_H_

