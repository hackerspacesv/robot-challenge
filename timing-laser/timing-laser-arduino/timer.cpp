#include <Arduino.h>

#include "timer.h"

#define TRIGGER_DEAD_TIME 5000L

TIMER::TIMER(BUBBLE_DISPLAY &display, int led_pin):
//The timer constructor sets the LED pin, sets the bubble display reference, starts the FSM in idle
//state, sets an initial lap limit of 3, sets lap count to 0, sets the time shown to 0 (total time),
//clears the manual stop flag and clears all registered times for trigger and lap time.
led_pin(led_pin), display(display), state(STATE_IDLE),
lap_limit(3), lap_count(0), time_shown(0),
manual_stop(false),
last_trigger_time(0), lap_time{ 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L }
{
}

void TIMER::update() {
  //Capture the current millis time count.
  unsigned long current_time = millis();

  //Update outputs based on FSM state.
  switch (state) {
    case STATE_IDLE:
      //Display the programmed lap limit.
      update_display_laps();

      //Blink the LED twice aproximately every 2 seconds.
      digitalWrite(led_pin, ((current_time & 0x000007C0) == 0x00000040) |
                            ((current_time & 0x000007C0) == 0x00000140));
      break;
    case STATE_RUN:
      //Check wether the maximum time count has been reached. 
      if (current_time - lap_time[0] > 99L * 10 + 59L * 1000L + 9L * 60L * 1000L) {
        //Force the current time so that the difference from the start time is 9:59.99.
        current_time = lap_time[0] + 99L * 10 + 59L * 1000L + 9L * 60L * 1000L;

        //Register the time for this forced last lap, set time shown to total and stop the timer.
        lap_time[++lap_count] = current_time;
        time_shown = 0;
        state = STATE_STOP;
      }

      //Display the current total time.
      update_display_time(lap_count + 1, current_time - lap_time[0]);

      //Blink the LED once aproximately every second.
      digitalWrite(led_pin, (current_time & 0x00000200) == 0x00000200);
      break;
    case STATE_STOP:
      //Check wether time shown is 0 or any other value.
      if (time_shown == 0)
        //Time shown is 0 (total time). Display it accordingly.
        update_display_time('A', lap_time[lap_count] - lap_time[0]);
      else
        //Time shown is other than 0 (lap time). Display it accordingly.
        update_display_time(time_shown, lap_time[time_shown] - lap_time[time_shown - 1]);

      //LED is always on during this state.
      digitalWrite(led_pin, true);
      break;
  }
}

void TIMER::trigger() {
  //Capture the current millis time count.
  unsigned long current_time = millis();

  //Ignore trigger signals that come before the dead time (filters trigger glitches).
  if (current_time - last_trigger_time < TRIGGER_DEAD_TIME)
    return;

  //Update the trigger time.
  last_trigger_time = current_time;

  //Perform actions based on FSM state.
  switch (state) {
    case STATE_IDLE:
      //A trigger during IDLE state captures the current time as the start time and sets the FSM
      //state to RUN.
      lap_time[0] = current_time;
      state = STATE_RUN;
      break;
    case STATE_RUN:
      //A trigger during the RUN state captures the current time as a lap time.
      lap_time[++lap_count] = current_time;

      //If the lap count reaches the programmed lap limit, set the time shown to 0 (total time) and
      //set the FSM state to STOP.
      if (lap_count >= lap_limit) {
        time_shown = 0;
        state = STATE_STOP;
      }

      break;
    case STATE_STOP:
      //A trigger performs no actions during the STOP state. The reset button has to be activated in
      //order to leave this state.
      break;
  }
}

void TIMER::on_button_reset() {
  switch (state) {
    case STATE_IDLE:
      //Reset button does nothing during IDLE state.
      break;
    case STATE_RUN:
      //Reset button causes the timer to stop prematurely during RUN state. Capture the last lap
      //time, set the manual stop flag and set the FSM state to STOP.
      lap_time[++lap_count] = millis();
      manual_stop = true;
      state = STATE_STOP;
      break;
    case STATE_STOP:
      //Reset button causes the timer to restart and become ready during STOP state. Reset the lap
      //count, clear the manual stop flag, clear all lap times and set the FSM state to IDLE.
      lap_count = 0;
      manual_stop = false;

      for (uint8_t i = 0; i < 11; i++)
        lap_time[i] = 0;

      state = STATE_IDLE;
      break;
  }
}

void TIMER::on_button_lap() {
  switch (state) {
    case STATE_IDLE:
      //Lap button adjusts the programmed lap limit from 1 to 9 laps during IDLE state.
      ++lap_limit;
      if (lap_limit > 9)
        lap_limit = 1;
      break;
    case STATE_RUN:
      //Lap button does nothing during RUN state.
      break;
    case STATE_STOP:
      //Lap button selects the time shown between 0 (total) and the lap count during STOP state.
      ++time_shown;
      if (time_shown > lap_count)
        time_shown = 0;
      break;
  }
}

bool TIMER::read_register(uint16_t address, uint32_t *data) {
  if (address == 0x0000) {
    //I32CTT register address 0 is status.
    //Bitfields:
    //[2..0]: lap limit.
    //[6..4]: lap count.
    //[8]: manual stop.
    *data = ((((uint32_t) lap_limit & 0x0000000F) << 0) |
             (((uint32_t) lap_count & 0x0000000F) << 4) |
             (manual_stop? (1 << 8): 0));

    //Register read successfully.
    return true;
  }
  else if (address == 0x0001) {
    //I32CTT register address 1 is total time in milliseconds.
    if (state == STATE_RUN)
      //During RUN state calculate total time dinamycally by subtracting the start time.
      *data = millis() - lap_time[0];
    else
      //During other states total time is obtained from stored times.
      *data = lap_time[lap_count] - lap_time[0];

    //Register read successfully.
    return true;
  }
  else if (address >= 2 && address <= 10) {
    //I32CTT register addresses 2 through 10 are lap times.
    *data = lap_time[address - 1] - lap_time[address - 2];

    //Register read successfully.
    return true;
  }
  else {
    //Invalid register.
    return false;
  }
}

void TIMER::update_display_time(uint8_t lap, unsigned long time) {
  uint8_t digit_count;
  uint8_t i;
  char display_digit[8];

  //First 3 digits contain the lap number and a dash.
  display_digit[0] = 'L';
  display_digit[1] = lap;
  display_digit[2] = '-';

  //Calculate the digit for units of minutes.
  for (digit_count = 0; time >= 1L * 60L * 1000L;
       time -= 1L * 60L * 1000L, ++digit_count);

  display_digit[3] = digit_count;

  //Calculate the digit for tens of seconds.
  for (digit_count = 0; time >= 10L * 1000L;
       time -= 10L * 1000L, ++digit_count);

  display_digit[4] = digit_count;

  //Calculate the digit for units of seconds.
  for (digit_count = 0; time >= 1L * 1000L;
       time -= 1L * 1000L, ++digit_count);

  display_digit[5] = digit_count;

  //Calculate the digit for tens of seconds hundredths.
  for (digit_count = 0; time >= 1L * 100L;
       time -= 1L * 100L, ++digit_count);

  display_digit[6] = digit_count;

  //Calculate the digit for units of seconds hundredths.
  for (digit_count = 0; time >= 1L * 10L;
       time -= 1L * 10L, ++digit_count);

  display_digit[7] = digit_count;

  //Update all decimal points before the digits.
  for (i = 0; i < 8; i++) {
    if (i == 3)
      //Decimal point after 4th digit is always on.
      display.set_dp(i, true);
    else if (i == 5)
      //Decimal point after 6th digit is always on during STOP or blinking each second during RUN.
      display.set_dp(i, state == STATE_STOP || display_digit[6] < 5);
    else
      //For all other digits, decimal point is always off.
      display.set_dp(i, false);
  }

  //Update all display digits.
  for (i = 0; i < 8; i++)
    display.set_digit(i, display_digit[i]);
}

void TIMER::update_display_laps() {
  //Clear all display decimal points.
  for (uint8_t i = 0; i < 8; i++)
    display.set_dp(i, false);

  //Draw on the display the currently configured lap limit.
  display.set_digit(0, 'L');
  display.set_digit(1, 'A');
  display.set_digit(2, 'P');
  display.set_digit(3, 'S');
  display.set_digit(4, ' ');
  display.set_digit(5, '-');
  display.set_digit(6, lap_limit);
  display.set_digit(7, '-');
}
